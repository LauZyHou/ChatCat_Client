# CatCat聊天客户端
ChatCat聊天(客户端)，Swing+Socket(TCP)，JavaSE课程项目。 
## 主要界面展示
### 登录界面
![](https://i.imgur.com/lwZRWZM.jpg)
### 各类使用界面
![](https://i.imgur.com/iwMWf9G.jpg)

![](https://i.imgur.com/XOeLugV.jpg)

![](https://i.imgur.com/goBsZ4U.jpg)

![](https://i.imgur.com/CfeGwvc.jpg)

![](https://i.imgur.com/FGC8OIj.jpg)

![](https://i.imgur.com/OYlrvpZ.jpg)

![](https://i.imgur.com/XktoKQT.jpg)
### 注册界面
![](https://i.imgur.com/aUDfqUL.jpg)
## 开发与备注日志
### 2018年8月12日
【1】今日添加README文件。
<br>
【2】ChatCat的客户端、数据库、服务端分离在不同的主机上，需要一台路由器建立局域网，在局域网内实现多线程聊天。
<br>
【3】服务端见[ChatCat聊天服务端](https://github.com/LauZyHou/ChatCat_Server)。
<br>
【4】数据库文件在`chatcat_client/mysql_bash/`目录下。
